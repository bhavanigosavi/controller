package com.sample.demo.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.http.MediaType;

import com.sample.demo.StudentDb;
import com.sample.demo.service.StudentService;

@RestController
public class StudentController {
	List<StudentDb> list = new ArrayList<StudentDb>();
	@Autowired
	StudentService studentService;

	@GetMapping(value = "/students", produces = { MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<List<StudentDb>> getStudents(@RequestParam(name = "age", required = false) Integer age) {
		List<StudentDb> students = studentService.getAllStudents();
		return ResponseEntity.status(200).body(students);
	}

	@PostMapping("students")
	public ResponseEntity<StudentDb> saveStudent(@RequestBody StudentDb students) {
		return ResponseEntity.status(201).body(studentService.saveStudent(students));
	}

	@PutMapping("students/{id}")
	public ResponseEntity<StudentDb> updateStudent(@PathVariable("id") Integer id, @RequestBody StudentDb student) {
		studentService.updateStudent(id, student);
		return ResponseEntity.status(200).body(student);
	}

	@PatchMapping("students/{id}")
	public ResponseEntity<StudentDb> partialUpdateStudent(@PathVariable("id") Integer id,
			@RequestBody StudentDb student) {
		StudentDb updatedStudent = studentService.partialStudentUpdate(id, student);
		return ResponseEntity.status(200).body(updatedStudent);
	}

	@DeleteMapping("students/{id}")
	public void deleteStudent(@PathVariable("id") Integer id) {
		studentService.deleteStudent(id);
		// return ResponseEntity.status(201).body(List<StudentDb>);

	}

	@GetMapping(value = "/students10", produces = { MediaType.APPLICATION_JSON_VALUE, })
	public ResponseEntity<List<StudentDb>> getStudentsage(@RequestParam(name = "age", required = false) Integer age) {
		if (age != null) {
			List<StudentDb> students = studentService.findStudentByAge(age);
			return ResponseEntity.status(200).body(students);
		} else {
			List<StudentDb> students = studentService.getAllStudents();
			return ResponseEntity.status(200).body(students);

		}
	}

	@GetMapping(value = "/students1", produces = { MediaType.APPLICATION_JSON_VALUE, })
	public ResponseEntity<List<StudentDb>> getStudentsid(@RequestParam(name = "id", required = false) Integer id) {
		if (id != null) {
			List<StudentDb> students = studentService.findStudentById(id);
			return ResponseEntity.status(200).body(students);
		} else {
			List<StudentDb> students = studentService.getAllStudents();
			return ResponseEntity.status(200).body(students);

		}
	}

	@GetMapping(value = "/students5", produces = { MediaType.APPLICATION_JSON_VALUE, })
	public ResponseEntity<List<StudentDb>> getStudentsByAgeGreaterThanEqual(
			@RequestParam(name = "age", required = false) Integer age) {
		if (age != null) {
			List<StudentDb> students = studentService.findStudentByAgeGreaterThanEqual(age);
			return ResponseEntity.status(200).body(students);
		} else {
			List<StudentDb> students = studentService.getAllStudents();
			return ResponseEntity.status(200).body(students);

		}
	}

	@GetMapping(value = "/students9", produces = { MediaType.APPLICATION_JSON_VALUE, })
	public ResponseEntity<List<StudentDb>> getStudentsByAgeBetweenl(
			@RequestParam(name = "fromAge", required = true) Integer fromAge,
			@RequestParam(name = "toAge", required = true) Integer toAge) {
		List<StudentDb> students = studentService.findStudentByAgeBetween(fromAge, toAge);
		return ResponseEntity.status(200).body(students);
	}
}
