package com.sample.demo.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.sample.demo.StudentDb;

public interface StudentRepository extends JpaRepository<StudentDb, Integer> {
	public List<StudentDb> findByAge(int age);

	public List<StudentDb> findById(int id);

	public List<StudentDb> findByAgeLessThanEqual(int age);

	public List<StudentDb> findByAgeGreaterThanEqual(int age);

	public List<StudentDb> findByAgeBetween(int from, int to);
}
