package com.sample.demo.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sample.demo.StudentDb;
import com.sample.demo.Util;
import com.sample.demo.repository.StudentRepository;

@Service
public class StudentService {
	@Autowired
	private StudentRepository studentRepository;

	public StudentDb saveStudent(StudentDb student) {
		return studentRepository.save(student);
	}

	public List<StudentDb> getAllStudents() {
		return studentRepository.findAll();
	}

	public StudentDb updateStudent(Integer id, StudentDb student) {
		student.setId(id);
		return studentRepository.save(student);
	}

	public StudentDb partialStudentUpdate(Integer id, StudentDb student) {
		student.setId(id);
		Optional<StudentDb> studentFromDb = studentRepository.findById(id);
		if (studentFromDb.isPresent()) {
			StudentDb temp = studentFromDb.get();
			// Util.myCopyProperties(src, target);
			Util.myCopyProperties(student, temp);
			return studentRepository.save(temp);
		} else {
			return null;
		}
	}

	public void deleteStudent(Integer id) {
		studentRepository.deleteById(id);
	}

	public List<StudentDb> findStudentByAge(int age) {
		return studentRepository.findByAge(age);
	}

	public List<StudentDb> findStudentById(int id) {
		return studentRepository.findById(id);
	}

	public List<StudentDb> findStudentByAgeGreaterThanEqual(int age) {
		return studentRepository.findByAgeGreaterThanEqual(age);
	}

	public List<StudentDb> findStudentByAgeBetween(int from, int to) {
		return studentRepository.findByAgeBetween(from, to);
	}

}
